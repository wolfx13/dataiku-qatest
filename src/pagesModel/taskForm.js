import {t} from 'testcafe';
import {TaskDashboardSelectors} from "../selectors/tasksDashboard.selectors";

class TaskForm {

    async addNewTask(taskData) {
        await t
            .click(TaskDashboardSelectors.page.buttons.addTask);

        if(taskData.title !== undefined){
            await t
                .click(TaskDashboardSelectors.page.addForm.inputs.taskTitle)
                .typeText(TaskDashboardSelectors.page.addForm.inputs.taskTitle, taskData.title, {paste: true, replace: true})
        }
        if(taskData.tag !== undefined){
            await t
                .click(TaskDashboardSelectors.page.addForm.inputs.taskTag)
                .typeText(TaskDashboardSelectors.page.addForm.inputs.taskTag, taskData.tag, {paste: true, replace: true})
        }

        await t.click(TaskDashboardSelectors.page.addForm.buttons.addTask)
    }

    async editTask(taskData) {
        await t
            .click(TaskDashboardSelectors.page.buttons.editTask);

        if(taskData.title !== undefined){
            await t
                .click(TaskDashboardSelectors.page.editForm.inputs.taskTitle)
                .typeText(TaskDashboardSelectors.page.editForm.inputs.taskTitle, taskData.title, {paste: true, replace: true})
        }

        if(taskData.markAsDone !== undefined){
            await t
                .click(TaskDashboardSelectors.page.editForm.buttons.checkDone)
                .typeText(TaskDashboardSelectors.page.editForm.inputs.taskTag, taskData.markAsDone)
        }

        if(taskData.tag !== undefined){
            await t
                .click(TaskDashboardSelectors.page.editForm.inputs.taskTag)
                .typeText(TaskDashboardSelectors.page.editForm.inputs.taskTag, taskData.tag, {paste: true, replace: true})
        }

        await t.click(TaskDashboardSelectors.page.editForm.buttons.editTask)
    }

    async deleteTask() {
        await t
            .click(TaskDashboardSelectors.page.buttons.deleteTask);
    }

    async markAsDoneTask() {
        await t
            .click(TaskDashboardSelectors.page.buttons.markAsDoneTask);
    }

    async markInProgressTask() {
        await t
            .click(TaskDashboardSelectors.page.buttons.markAsInProgressTask);
    }
}

export default new TaskForm();