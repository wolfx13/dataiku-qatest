import {t} from 'testcafe';
import {LoginSelectors} from "../selectors/login.selectors";
import {config} from '../../config/config';

class Login {

    async chooseLoginName(name, password) {

        await t
            .click(LoginSelectors.page.inputs.username)
            .typeText(LoginSelectors.page.inputs.username, name.toString(),
                { replace:true, paste: true })
            .click(LoginSelectors.page.inputs.password)
            .typeText(LoginSelectors.page.inputs.password, password.toString(),
                { replace:true, paste: true })
    }

    async pushOnLoginButton() {
        await t
            .click(LoginSelectors.page.buttons.login);
    }

    async pushOnLogoutButton() {
        await t
            .click(LoginSelectors.page.buttons.logout);
    }

    async openThePage() {
        await t
            .navigateTo(config.url)
            .maximizeWindow();
    }
}

export default new Login();