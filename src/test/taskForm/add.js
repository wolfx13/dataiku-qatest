import {login} from '../../helpers/login';
import {addNewTask} from "../../helpers/task";
import {TaskDashboardSelectors} from "../../selectors/tasksDashboard.selectors";
import {resetDb} from "../../utils/request";
import {Faker} from "../../utils/faker";
import faker from 'faker';

let dataSetFunctional = Faker.functionalTask();
let dataSetNoFunctional = Faker.noFunctionalTask();

fixture `Add`
    .beforeEach(async (t) => {
        const username = faker.name.firstName();
        const password = faker.name.lastName();
        await login(username, password);
    })
    .afterEach(async (t) =>{
        // Reset DB
        await resetDb();
    });

dataSetFunctional.forEach(task => {
    test(`${task.test}`, async (t) => {
        // Add Task
        await addNewTask(task);
        await t
            .expect(TaskDashboardSelectors.page.elements.title.innerText)
            .contains(task.title)
            .expect(TaskDashboardSelectors.page.elements.tag.innerText)
            .contains(task.tag)
    });
})

dataSetNoFunctional.forEach(task => {
    test(`${task.test}`, async (t) => {
        // Add Task
        await addNewTask(task);
        await t
            .expect(TaskDashboardSelectors.page.elements.title.exists)
            .notOk()
    });
})