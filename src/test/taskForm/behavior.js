import {login, logout} from '../../helpers/login';
import {addNewTask, markAsDone, markAsInProgress} from "../../helpers/task";
import {TaskDashboardSelectors} from "../../selectors/tasksDashboard.selectors";
import {resetDb} from "../../utils/request";
import {Faker} from "../../utils/faker";
import faker from 'faker';

let dataSet = Faker.newTagAndTask();

fixture `Behaviors`
    .beforeEach(async (t) => {
        const username = faker.name.firstName();
        const password = faker.name.lastName();
        await login(username, password);
    })
    .afterEach(async (t) =>{
        // Reset DB
        await resetDb();
    });

test.skip(`Check two tag insertion`, async (t) => {
    // Add Task
    await addNewTask(dataSet[2]);

    const test = dataSet[2].tag.split(" ");
    const iframeAmount = await TaskDashboardSelectors.page.elements.tag.count;

    for (const element of test) {
        for (let i = 0; i < iframeAmount; i++){
            const node = await TaskDashboardSelectors.page.elements.tag.nth(i).innerText;
            if(node === element){
                await t
                    .expect(TaskDashboardSelectors.page.elements.tag.nth(i).innerText).contains(element)
            }
        }
    }

});

test.skip(`Mark as Done`, async (t) => {
    // Add Task
    await addNewTask(dataSet[0]);
    await markAsDone();
    await t
        .expect(TaskDashboardSelectors.page.elements.done.visible)
        .ok()
});

test.skip(`Mark as In progress`, async (t) => {
    // Add Task
    await addNewTask(dataSet[0]);
    await markAsDone();
    await t
        .expect(TaskDashboardSelectors.page.elements.done.visible)
        .ok()

    await markAsInProgress();
    await t
        .expect(TaskDashboardSelectors.page.elements.inProgress.visible)
        .ok()
});


test(`Should not edit/delete/done task from an other user`, async (t) => {
    // Add Task
    await addNewTask(dataSet[0]);
    await logout();

    const current_url = await t.eval(() => window.location.href);
    await t.navigateTo(current_url);

    const username = "yeah";
    const password = "surpise";
    await login(username, password);

    await t
        .expect(TaskDashboardSelectors.page.buttons.deleteTask.visible)
        .notOk()
        .expect(TaskDashboardSelectors.page.buttons.editTask.visible)
        .notOk()
        .expect(TaskDashboardSelectors.page.buttons.markAsDoneTask.visible)
        .notOk()
});