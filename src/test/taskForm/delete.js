import {login} from '../../helpers/login';
import {addNewTask, deleteTask, markAsDone, markAsInProgress} from "../../helpers/task";
import {TaskDashboardSelectors} from "../../selectors/tasksDashboard.selectors";
import {resetDb} from "../../utils/request";
import {Faker} from "../../utils/faker";
import faker from 'faker';

let dataSet = Faker.newTagAndTask();

fixture `Delete`
    .beforeEach(async (t) => {
        const username = faker.name.firstName();
        const password = faker.name.lastName();
        await login(username, password);
    })
    .afterEach(async (t) =>{
        // Reset DB
        await resetDb();
    });

test(`Delete task`, async (t) => {
    // Add Task
    await addNewTask(dataSet[0]);
    await t
        .expect(TaskDashboardSelectors.page.elements.title.innerText)
        .contains(dataSet[0].title);

    // Delete task
    await deleteTask();
    await t
        .expect(TaskDashboardSelectors.page.elements.title.exists)
        .notOk();
});