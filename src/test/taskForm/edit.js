import {login} from '../../helpers/login';
import {addNewTask, editTask} from "../../helpers/task";
import {TaskDashboardSelectors} from "../../selectors/tasksDashboard.selectors";
import {resetDb} from "../../utils/request";
import {Faker} from "../../utils/faker";
import faker from 'faker';

let dataSetFunctionalTitleOnly = Faker.editTitleOfFunctionalTask();
let dataSetFunctionalTagOnly = Faker.editTagOfFunctionalTask();
let dataSetNoFunctional = Faker.editTitleOfFunctionalTaskShouldNotBeOk();

fixture `Edit`
    .disablePageCaching
    .beforeEach(async (t) => {
        const username = faker.name.firstName();
        const password = faker.name.lastName();
        await login(username, password);
    })
    .afterEach(async (t) =>{
        // Reset DB
        await resetDb();
    });

dataSetFunctionalTitleOnly.forEach(task => {
    test(`Edit task: ${task.test}`, async (t) => {
        // Add Task
        await addNewTask(task);
        // Edit task
        await editTask(task.edit);

        const current_url = await t.eval(() => window.location.href);
        await t.navigateTo(current_url);

        await t
            .expect(TaskDashboardSelectors.page.elements.title.innerText)
            .contains(task.edit.title)
    });
})

dataSetNoFunctional.forEach(task => {
    test(`Edit task: ${task.test}`, async (t) => {
        // Add Task
        await addNewTask(task);
        // Edit task
        await editTask(task.edit);

        const current_url = await t.eval(() => window.location.href);
        await t.navigateTo(current_url);

        await t
            .expect(TaskDashboardSelectors.page.elements.title.innerText)
            .notContains(task.edit.title)
    });
})

dataSetFunctionalTagOnly.forEach(task => {
    test(`Edit task: ${task.test}`, async (t) => {
        // Add Task
        await addNewTask(task);

        // Edit task
        await editTask(task.edit);

        const current_url = await t.eval(() => window.location.href);
        await t.navigateTo(current_url);

        await t
            .expect(TaskDashboardSelectors.page.elements.tag.innerText)
            .contains(task.edit.tag)
    });
})