import {login} from '../../helpers/login';
import {config} from '../../../config/config';
import {TaskDashboardSelectors} from "../../selectors/tasksDashboard.selectors";
import {LoginSelectors} from "../../selectors/login.selectors";

fixture `Logout from the form`;

test(`Should be able to see Add task and logout button after login`, async (t) => {
    await login(config.username, config.password);
    await t
        .expect(TaskDashboardSelectors.page.buttons.addTask.exists).ok()
        .expect(LoginSelectors.page.buttons.logout.exists).ok();
});
