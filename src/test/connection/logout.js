import {login, logout} from '../../helpers/login';
import {config} from '../../../config/config';
import {LoginSelectors} from "../../selectors/login.selectors";
import {TaskDashboardSelectors} from "../../selectors/tasksDashboard.selectors";

fixture `Logout from the form`;

    test(`Should not be able to see Add task and logout button after logout`, async (t) => {
        await login(config.username, config.password);
        await logout();
        await t
            .expect(TaskDashboardSelectors.page.buttons.addTask.visible).notOk()
            .expect(LoginSelectors.page.buttons.logout.visible).notOk();
    });
