import { cs } from '../utils/selector';

const add = cs('[id="add"]');
const edit = cs('[id="edit"]');

export const TaskDashboardSelectors = {
    page: {
        buttons: {
            addTask: cs('#btn-add'),
            editTask: cs('[data-bind="click: $parent.beginEdit, visible: $parent.authenticated() && $parent.username() == username()"]'),
            deleteTask: cs('[data-bind="click: $parent.remove, visible: $parent.authenticated() && $parent.username() == username()"]'),
            markAsDoneTask: cs('[data-bind="click: $parent.markDone"]'),
            markAsInProgressTask: cs('[data-bind="click: $parent.markInProgress"]')
        },
        addForm: {
            inputs: {
                taskTitle: add.find('[data-bind="value: title"]'),
                taskTag:  add.find('[data-bind="value: tags"]'),
            },
            buttons:{
                addTask: cs('[data-bind="click:addTask"]'),
                taskClose:  add.find('[class="btn btn-default"]'),
                taskCloseCross:  add.find('[class="modal-header"] [data-dismiss="modal"]'),
            }
        },
        editForm: {
            inputs: {
                taskTitle: edit.find('[data-bind="value: title"]'),
                taskTag: edit.find('[data-bind="value: tag.name"]'),
            },
            buttons:{
                editTask: cs('[data-bind="click:editTask"]'),
                taskClose: edit.find('[class="btn btn-default"]'),
                taskCloseCross: edit.find('[class="modal-header"] [data-dismiss="modal"]'),
                checkDone: cs('[data-bind="checked: done"]')
            }
        },
        elements: {
            done: cs('[data-bind="visible: done"]'),
            inProgress: cs('[data-bind="visible: !done()"]'),
            username: cs('[data-bind="text: username"]'),
            title: cs('[data-bind="text: title"]'),
            tag: cs('[data-bind="text: tag.name"]'),
            date: cs('[data-bind="text: date"]'),
        }
    }

}
