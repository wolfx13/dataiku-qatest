import { cs } from '../utils/selector';

export const LoginSelectors = {
    page: {
        inputs: {
            username: cs('[name="username"]'),
            password: cs('[name="password"]')
        },
        buttons: {
            login: cs('[data-bind="click: authenticate"]'),
            logout: cs('[data-bind="click: logout"]')
        }
    }

}
