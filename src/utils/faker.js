import faker from 'faker';

export class Faker {

  static functionalTask() {
    return [{
      test: 'Classic Functional Add task',
      title: faker.name.firstName(),
      tag: faker.name.lastName(),
    }];
  }

  static editTitleOfFunctionalTask() {
    return [{
      test: 'Edit Title of Functional task',
      title: faker.name.firstName(),
      tag: faker.name.lastName(),
      edit:{
        title: faker.name.firstName(),
      }
    },{
      test: 'Edit Title of Functional task with a space between word',
      title: faker.name.firstName(),
      tag: faker.name.lastName(),
      edit:{
        title: faker.name.findName(),
      }
    },{
      test: 'Edit Title of Functional task with special character',
      title: faker.name.firstName(),
      tag: faker.name.lastName(),
      edit:{
        title: faker.name.firstName() + '(for the horde)',
      }
    }];
  }

  static editTitleOfFunctionalTaskShouldNotBeOk() {
    return [{
      test: 'Edit Title of Functional task with more than 40 characters',
      title: faker.name.firstName(),
      tag: faker.name.lastName(),
      edit:{
        title: faker.lorem.words(40),
      }
    }];
  }

  static editTagOfFunctionalTask() {
    return [{
      test: 'Edit tag from Functional task with a new name',
      title: faker.name.firstName(),
      tag: faker.name.lastName(),
      edit:{
        tag: faker.name.firstName(),
      }
    },{
      test: 'Edit tag Functional task for two tags',
      title: faker.name.firstName(),
      tag: faker.name.lastName(),
      edit:{
        tag: faker.name.findName(),
      }
    },{
      test: 'Edit tag Functional with a large name',
      title: faker.name.firstName(),
      tag: faker.name.lastName(),
      edit:{
        tag: faker.lorem.words(40),
      }
      },{
      test: 'Edit tag Functional with special characters',
      title: faker.name.firstName(),
      tag: faker.name.lastName(),
      edit:{
        tag: "é&'('ç_è-(",
      }
    },
    ]
  }

  static noFunctionalTask() {
    return [{
      test: 'Create Task without taskName',
      tag: faker.name.lastName(),
    },{
      test: 'Create Task without taskTag',
      title: faker.name.firstName(),
    },{
      test: 'Create Task with spécial character in the title',
      title: faker.name.firstName() + '(çàç_)',
    },{
      test: 'Create Task with compose title',
      title: faker.name.findName(),
    },{
      test: 'Create Task with a title who contain more than 20 characters',
      title: faker.lorem.words(30),
    },{
      test: 'Create Task with spécial character in the tag',
      tag: faker.name.firstName()  + '(çàç_)',
    },];
  }

  /**
   *
   * @returns
   */
  static newTagAndTask() {
    return [{
      test: 'Classic Functional Add task',
      title: faker.name.firstName(),
      tag: faker.name.lastName(),
      edit:{
        title: faker.name.firstName(),
        tag: faker.name.lastName(),
        markAsDone: true
      }
    },{
      test: 'Classic Add task',
      title: faker.name.firstName(),
      tag: faker.name.lastName(),
      edit:{
        title: faker.name.firstName(),
        tag: faker.name.lastName(),
        markAsDone: true
      }
    },{
      test: 'Check two tag insertion',
      title: faker.name.firstName(),
      tag: "a b",
      edit:{
        tag: faker.name.lastName(),
        markAsDone: false
      }
    }];
  }
}
