import { Selector } from 'testcafe';

/**
 *
 * @param {string|Selector|NodeSnapshot|SelectorPromise|Node|[Node]|NodeList|HTMLCollection} init
 * @param {SelectorOptions=} options
 * @returns {Selector & {css}}
 * @constructor
 */
export function cs(init, options) {
  return Object.assign( Selector(init, options), { css: init });
}
