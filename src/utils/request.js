import { Selector } from 'testcafe';

const axios = require('axios')

/**
 *
 * @param {string|Selector|NodeSnapshot|SelectorPromise|Node|[Node]|NodeList|HTMLCollection} init
 * @param {SelectorOptions=} options
 * @returns {Selector & {css}}
 * @constructor
 */
export function cs(init, options) {
    return Object.assign( Selector(init, options), { css: init });
}


export function createUser (username, password) {
    return new Promise(resolve => {
        axios
            .post('http://roche.qatest.dataiku.com/users', {
                username: username,
                password: password
            })
            .then(res => {
                //console.log(`statusCode: ${res.status}`)
                //console.log(res)
                resolve();
            })
            .catch(error => {
                console.error(error)
                resolve();
            })
    });
}

export function resetDb () {
    return new Promise(resolve => {
        axios
            .get('http://roche.qatest.dataiku.com/reset')
            .then(res => {
                //console.log(`statusCode: ${res.status}`)
                //console.log(res)
                resolve();
            })
            .catch(error => {
                console.error(error)
                resolve();
            })
    });
}