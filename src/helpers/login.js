import loginComponent from "../pagesModel/login";
import {createUser} from "../utils/request";

export async function login(loginName, loginPassword) {

    await createUser(loginName, loginPassword);
    await loginComponent.openThePage();
    await loginComponent.chooseLoginName(loginName, loginPassword);
    await loginComponent.pushOnLoginButton();
}

export async function logout() {
    await loginComponent.pushOnLogoutButton();
}