import taskForm from "../pagesModel/taskForm";

export async function addNewTask(taskData) {
    await taskForm.addNewTask(taskData);
}

export async function editTask(taskData) {
    await taskForm.editTask(taskData);
}

export async function deleteTask() {
    await taskForm.deleteTask();
}

export async function markAsDone() {
    await taskForm.markAsDoneTask();
}

export async function markAsInProgress() {
    await taskForm.markInProgressTask();
}

