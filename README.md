## Getting Started

This is a little test for Dataiku enterprise

### Installation

1. Install Nodejs
   ```sh
   https://nodejs.org/en/download/
   ```
2. Install NPM packages
   ```sh
   npm install
   ```

<!-- USAGE EXAMPLES -->
## Usage

Enter your information in `config.local.js`

1. Launch testcafe file
   ```JS
   node_modules/.bin/testcafe 'chrome:headless' src/test/connection/*.js -S -s screenshots -e --selector-timeout 10000
   node_modules/.bin/testcafe 'chrome:headless' src/test/taskForm/*.js -S -s screenshots -e --selector-timeout 10000
   ```
    You can find this file in `src/test/*.js`


2. Postman Collection
   To use it, you might install "newman" with ``npm install -g newman``

   ```JS
   postmanCollection/dataiku-qaTest.postman_collection.json
   postmanCollection/dataiku-qatest-environnement.postman_environment.json
   ```
   You have to add your server/username/password into the `postman_environement` file than launch the command:
   ```JS
   newman run postmanCollection/dataiku-qaTest.postman_collection.json -e postmanCollection/dataiku-qatest-environnement.postman_environment.json
   ```